//validate doi, references=true and way to get percentage of free references.

//based entirely on node/services/lib/validation.js from JavaScript Testing Recipes, 2014, James Coglan.

module.exports = {

	VALID_DOI: /\b(10[.][0-9]{4,}(?:[.][0-9]+)*\/(?:(?!["&\'<>])\S)+)\b/, //regex yoinked from http://stackoverflow.com/questions/27910/finding-a-doi-in-a-document-or-page
	VALID_PMID: /[0-9]*/,
	VALID_JOURNAL: /[a-zA-Z]*/,//can contain spaces and periods  too

	DOI_ERROR: "Format of DOI is not valid",
	PMID_ERROR: "PMID may only contain numbers",
	JOURNAL_ERROR: "Journal title may only contain letters, or can it?",


	checkDoi: function(doiData){
		var doi = doiData.doi || "",
		errors = []

		if(!doi.match(this.VALID_DOI)) errors.push(this.DOI_ERROR)

		return (errors.length === 0) ? null : errors
	},

	checkPmid: function(pmidData){
		var pmid = pmidData.pmid || "",
		errors = []

		if(!pmid.match(this.VALID_PMID)) errors.push(this.PMID_ERROR)

		return (errors.length === 0) ? null : errors
	},

	checkJournal : function(journalData){
		var journal = journalData.journal || "",
		errors = []

		if(!journal.match(this.VALID_JOURNAL)) errors.push(this.JOURNAL_ERROR)

		return (errors.length ===0) ? null : errors
	}
}