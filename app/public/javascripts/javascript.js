function appendResult(response){

	var ul = document.getElementById("result");
	var documentFragment = document.createDocumentFragment();
		
	//set up button to show free refs
	var showFreeRefs = document.createElement("input");
	showFreeRefs.setAttribute("type", "submit");
	showFreeRefs.setAttribute("value", "show free references");
	showFreeRefs.setAttribute("id", "showFreeRefs");
	showFreeRefs.addEventListener("click", getFreeRefs, false);

	//set up ul in which to append each reference in a li
	var refResults = document.createElement("table");
	refResults.setAttribute("id", "references")
	
	var key = Object.keys(response.response);
	
	for(var i = 0, l = key.length; i < l; i++){
		var li = document.createElement("li");
		//alert(key[i])
		var field = key[i];
		li.setAttribute("id", field);
		li.textContent = key[i] + ": "+ response.response[field];
		documentFragment.appendChild(li);
	}


	//if field is result, do not attache showFreeRefs
	if(key[0]!= "result"){
	documentFragment.appendChild(showFreeRefs);
	documentFragment.appendChild(refResults);
	}//omg

	ul.appendChild(documentFragment);

}


function displayRefs(response){

	//setup elements for document fragment
	var references = document.getElementById("references")
	var fragment = document.createDocumentFragment();
	
	//parse response
	var article = response.response["article"];
	var allFields = article.pop()

	var referenceResults = allFields["references"];
	

	referenceResults.forEach(function(obj){
		var tr = document.createElement("tr");

		var td1 = document.createElement("td");
		td1.textContent = obj["title"];
		tr.appendChild(td1);
		var td2 = document.createElement("td");
		//td2.textContent = obj["pmcid"]
		var link = document.createElement("a");
		link.setAttribute("href", "http://www.ncbi.nlm.nih.gov/pmc/articles/"+obj["pmcid"]+"/");
		link.setAttribute("target", "_blank");
		td2.appendChild(link);
		link.textContent = obj["pmcid"];
		tr.appendChild(td2);
		var td3 = document.createElement("td");
		td3.textContent = obj["doi"];
		tr.appendChild(td3);

		fragment.appendChild(tr);
	})

	
	//var keys = Object.keys(response.response);
	
	
	
	references.appendChild(fragment);
	//display refs somehow
}

function getFreeRefs(event){

	//var ul = document.getElementById("references")
	var doi = document.getElementById("doiValue").value;
	var encodedDoi = encodeURIComponent(doi);
	//alert("get references for "+ encodedDoi)
	//set up xhr request
	var request = new XMLHttpRequest();
	var url = "http://localhost:1337/api/v1/articles/doi/";
	var refParmas = "?references=free";

	function handleResponse(){
		if(request.readyState < 4){
			return;
		}

		if(request.status !== 200){
			alert("some error go figure");
			return;
		}

		//alert(request.responseText);
		displayRefs(request)//should be json

	}

	request.onreadystatechange = handleResponse;
	request.open("GET", url+encodedDoi+refParmas);
	request.responseType = "json";
	request.send(null);

	//displayRefs(request.response)
	event.preventDefault();

}



function notify(event){

	//if docFrag exists
	var cleanDocFrag = document.getElementById("result");
	while(cleanDocFrag.firstChild){
		cleanDocFrag.removeChild(cleanDocFrag.firstChild);
	}
	
	var doi = document.getElementById("doiValue").value;
	var request = new XMLHttpRequest();
	var url = "http://localhost:1337/api/v1/articles/doi/";
	var percentParams = "?references=free&count=percent";
	var encodedDoi = encodeURIComponent(doi);
	
	
	function reportRequest(){
		if(request.readyState < 4){
			return;
		}

		if(request.status !== 200){
			alert("some error go figure");
			return;
		}

		//alert(request.responseText);
		appendResult(request)//should be json
		

	}


	
	request.onreadystatechange = reportRequest;
	request.open("GET", url+encodedDoi+percentParams, true);
	request.responseType = "json";
	request.send(null);
	event.preventDefault();

}





//register event listners
var btn = document.getElementById("submitDoi");
var inputBox = document.getElementById("doiValue");
//inputBox follows btn's click behaviour and triggers notify without explicityly being told to do so.
//how does that happen?
btn.addEventListener("click", notify, false);
