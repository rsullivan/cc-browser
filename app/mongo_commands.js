//VERSION 2.4.9

//FIND DUPLICATE RECORDS

db.articlemodels.aggregate([{$group:{_id:{pmid:"$pmid"},uniqueIds: {$addToSet:"$_id"}, count:{$sum: 1} }}, {$match:{count:{$gte: 2}}}])

db.journalmodels.aggregate([{$group:{_id:{title:"$title"},uniqueIds: {$addToSet:"$_id"}, count:{$sum: 1} }}, {$match:{count:{$gte: 2}}}])

//FInd MAx and min values for a given key - 

//For example finding range of citation_count in order to organise into groups

//mapReduce

var map = function(){
	var x = { citations : this.citation_count, pmid : this.pmid};
	emit(this.title, {min: x, max: x})
	}
var reduce = function(key, values){
	 var res = values[0];
	for(var i=1; i<values.length; i++){
	if(values[i].min.citation_count < res.min.citation_count)
	res.min = values[i].min;
	if(values.max.citation_count > res.max.citation_count)
	res.max = values[i].max;
	}
	return res;
	}
	db.articlemodels.mapReduce(map, reduce, {query:{group:"base"},out:{inline:true}})



//return cursor and use cursor methods

var citations100_150 = db.articlemodels.find({citation_count:{$gt :100, $lt: 150}})
citations100_150.forEach(function(article){ print(article.pmid, article.citation_count) } )

//I have 1754 articles with a pubdate of <= 2012
//find an article's citations up to and including two years after publication
//quickest way to get article's citations is to search for where ObjectId appears in references field
//then count how many year values are less than or equal to pubYear of article.

//use forEach to go through results and get year.

var articlePubDate = 2011;

var citation_count_at_two_years = 0;

function matchPubYear(obj, index, array){
	console.log("array["+index+"] =" + obj["year"] )
	if(obj["year"] <= articlePubDate+2){
		citation_count_at_two_years++;
	}

}

ArticleModel.find({references: articleObjectId}, function(err, results){
	if(err){
		console.log(err)
	}else{
		results.forEach(matchPubYear)
		console.log(citation_count_at_two_years);
	}
})


//FIND CITATIONS THAT OCCURRED WITHIN TWO YEARS OF PUBDATE
//in the shell

var pub2012 = db.articlemodels.find({group:"base", citation_count:{$gt:0}, year: {$lte: 2012}})//limit

pub2012.forEach(function(article){ 
	var pubDate = article.year; 
	var citationsAtTwo = 0; 

	var articleCitations = db.articlemodels.find({references:article._id, year:{$gte:pubDate+2}}); //this query should work if every citation has a year, so far they don't.
	
    
    article.citation_count_two_years = articleCitations.count()
    db.articlemodels.save(article)
	//print(article.pmid +" has: "+articleCitations.count() + " within two years of publication")
	  
})//works - is slow but who cares?

//see also cursor map which collects the return values into an array. Can pmids of citations at two years be kept in array?

//eg. pmid:19481515 has 76 citations within two years


//FIND YEAR FIELD TYPE STRING AND CHANGE TO INT

var yearString = db.articlemodels.find({year:{$type:2}});

yearString.forEach(function(article){
        article.year = new NumberInt(article.year);
        db.articlemodels.save(article)
})

//find max value
db.articlemodels.find().sort({citation_count_at_two:1/-1}).limit(-1)//no idea why this works

//create groups based on citation_count_at_two field

//adding aritcles to journal's article field

var nature = db.journalmodels.find({title:"Nature"})

nature.forEach(function(journal){
	var articles = db.articlemodels.find({journal: journal._id})
	articles.forEach(function(article){
		db.journalmodels.update({journal:journal._id}, {$addToSet:{articles:article._id}})
	})
})

//or
nature.forEach(function(journal){
	var articles = db.articlemodels.find({journal:journal._id})
	var articleIds = articles.map(function(article){
		return article._id
		//db.journalmodels.update({_id:journal._id}, {$addToSet: {articles: article._id}})
	});
	db.journalmodels.update({_id:journal._id}, {$addToSet: {articles: {$each:articleIds}}})
	
})

//update citation_group field

var zero_citations = db.articlemodels.find({group:"base", citation_count_at_two: 0})
var hundred_plus = db.articlemodels.find({group:"base", citation_count_at_two:$gte:101});

hundred_plus.forEach(function(article){
	article.citation_group = "hundred_one_plus_citations"
	db.articlemodels.save(article)
})

//split into test and train data groups
db.articlemodels.find({group:"base",citation_group:"one_to_ten_citations"}).limit(480).forEach(
	function(article){
		article.sub_group = "test"
		db.articlemodels.save(article)
	})


// find distinct values of citation_group
db.articlemodels.distinct("citation_group")
//returns array of distinct values 
var citation_groups = db.articlemodels.distinct("citation_group");

citation_groups.forEach(function(group){
	var group_count = db.articlemodels.find({citation_group:group});
	print(group + " has " +group_count.count()+" articles");
})

// find all articles that all references cite and which references cite them.


//find if references cite a particular paper

// find articles that are cited by references of 10.1038/nature10158

//find all articles that are cited by the references of 10.1038/nature10158

var nature10158 = ObjectId("53a466dbd2a862b81a9e2782");

db.articlemodels.find({is_ref_of: nature10158}, {references:1})//scans all documents - bad.

var references = db.articlemodels.find({is_ref_of: nature10158}, {references:1})

references.map(function(refs){
	var allRefs = refs.references.pop()
})


var allRefs = references.map(function(refs){
	return refs.references;
});

//allRefs is array of arrays.

//already I can see that it may have to be recursive if I want to set the level at which I want to retrieve the source references
//this approach scans one record to begin with
var seed_references = db.articlemodels.find({doi:"10.1038/nature10158"}).map(function(article){
	return article.references
})

db.articlemodels.find({_id:{$in:seed_references}}).explain()//scanned 53 records


//now get all references for seed_references

//looking for matching values in reference field consider adding multikey index http://docs.mongodb.org/manual/core/index-multikey/#index-type-multikey

//find references
//find references of these references 
//use aggregate command on articlemodels collection and assign result to an object to manipulate
var results = db.articlemodels.aggregate([
	{$match:{is_ref_of:ObjectId("53a466dbd2a862b81a9e2782")}},
	{$group:{_id:"$references"}}
	])//array is pipeline

db.articlemodels.aggregate([
	{$match:{is_ref_of:ObjectId("53a466dbd2a862b81a9e2782")}},
	{$group:{_id:"$references", pmid:{$push:"$pmid"}}
	])//array is pipeline

//now I want to know if any duplicate ObjectIds exists in the group stage

//mapReduce outputs a new document takes ~400ms

var reduce = function(key, values){return values}//doesn't work on arrays.
var map = function(){emit(this.pmid, this.references)}
db.articlemodels.mapReduce(map, reduce, {query: {is_ref_of:ObjectId("53a466dbd2a862b81a9e2782")},out:"all_references"})

//straight ahead way
var seed_references = db.articlemodels.find({doi:"10.1038/nature10158"})
seed_references.forEach( 
	function(article){ 
		var refs = db.articlemodels.find({_id:{$in:article.references}}); 
		var count = 0;
		refs.forEach(
			function(article){ 
				count += article.references.length 
			}) 
		print(count)
	})