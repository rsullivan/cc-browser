var assert = require("chai").assert,
	validation = require("../lib/validation");



suite("validation", function(){

	test("checkDoi() accepts valid doi", function(){
		assert.isNull(validation.checkDoi({doi:"10.1534/g3.113.007690"}))

	});
	test("checkDoi() rejects invalid doi", function(){
		assert.sameMembers(["Format of DOI is not valid"], validation.checkDoi({doi:"11.78789/----"}))
	});
	test("checkDoi() rejects missing dois", function(){
		assert.sameMembers(["Format of DOI is not valid"], validation.checkDoi({}));
	});

});