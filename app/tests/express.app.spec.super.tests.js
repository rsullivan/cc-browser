var request = require("supertest"), //https://github.com/tj/supertest
	express = require("express");
	app = require("../app");

var mongoConfig = {host:"127.0.0.1", port:6379, db: "test"},
	server = app({mongoose:mongoConfig});

describe("GET /", function(){
	it("response is successful", function(done){
		request(server)
			.get("/")
			.expect(200)
			.end(function(err, res){
				if(err) throw err;
				
				done();
			})
	});
});



