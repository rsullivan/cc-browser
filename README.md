# README #

An api to retrieve reference and citation on articles in PubMed (http://www.ncbi.nlm.nih.gov/pubmed) and calculate what percentage of an article's references/citations are free to access.

### Purpose ###

1. Let researches find free articles
2. A learn how to code project


###Some instructions to get up and running (on Windows)###

1. Start the mongo server: `C:\mongodb\bin\mongod.exe`

2. In new prompt cd to app directory and set the following:

`set MONGOOSE_HOST=mongodb://localhost/`
`set MONGOOSE_DB=test`
`set SERVER_PORT=1337`

3. run command

`node main.js %MONGOOSE_HOST% %MONGOOSE_DB% %SERVER_PORT%`